package TemperatureConverter;

public class Converter {
    public double getTemperature(double temperature) {
        return (temperature * 9/5) + 32;
    }
}
